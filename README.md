# Satellite

![The main view with a GPS fix](https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot3.png)
![Logging](https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot2.png)
![Expanded satellite SNR view](https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot5.png)
![Speedometer and track recording](https://codeberg.org/tpikonen/satellite/raw/branch/main/data/screenshots/screenshot-track.png)

Satellite is an adaptive GTK / libhandy application which displays global navigation satellite system
(GNSS: GPS et al.) data obtained from modemmanager API. It can also save your position to a GPX-file.

## License

GPL-3.0

## Dependencies:

    python 3.6+, gi, Gtk, libhandy, pydbus, pynmea2, gpxpy

## Installing and running

### Flathub

<a href='https://flathub.org/apps/details/page.codeberg.tpikonen.satellite'>
<img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/>
</a>

Satellite is
[in flathub](https://flathub.org/apps/details/page.codeberg.tpikonen.satellite)
and can be installed from there, or from a software manager like Gnome software.
The direct install link is
[here](https://dl.flathub.org/repo/appstream/page.codeberg.tpikonen.satellite.flatpakref).

Run

    flatpak run page.codeberg.tpikonen.satellite

to execute from the command line.

### From source tree

Run the script `bin/satellite`.

### Pip from sources

Run

    pip3 install --user ./

in the source tree root.

This creates an executable Python script in `$HOME/.local/bin/satellite`.

### Flatpak from sources

Run

    flatpak-builder --install --user build-dir flatpak/page.codeberg.tpikonen.satellite.json

in the source tree root to install a local build to the user flatpak repo.

## Hints

You can start recording a GPX track by selecting 'Record track' from the main
menu. The GPX file is saved in `$HOME/Documents/satellite-tracks`.
